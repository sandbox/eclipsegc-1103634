<?php

$plugin = array(
  'title' => t('Workbench Content'),
  'description' => t('Deploys an instance of the workbench content administration tool.'),
  'content form' => 'workbench_workbench_content_settings_form',
  'content form submit' => 'workbench_workbench_content_settings_form_submit',
  'render' => 'workbench_workbench_content_render',
);

function workbench_workbench_content_settings_form($form, $form_state) {
  return $form;
}

function workbench_workbench_content_settings_form_submit($form, &$form_state) {
}

function workbench_workbench_content_render() {
  return workbench_content();
}

/**
 * Page callback for the workbench content page.
 *
 * Note that we add Views information to the array and render
 * the Views as part of the alter hook provided here.
 *
 * @see hook_workbench_content_alter()
 * 
 * @return
 *  A Render API array of content creation options.
 */
function workbench_content() {
  $output = array();

  // Allow other modules to add content here.
  $output['#attributes'] = array('class' => array('admin'));
  $output['#theme'] = 'workbench_element';
  $output['workbench_current_user'] = array(
    '#title' => t('My Profile'),
    '#view' => 'workbench_current_user',
    '#attributes' => array('class' => array('left', 'clearfix')),
    '#theme' => 'workbench_element',
  );
  $output['workbench_edited'] = array(
    '#title' => t('Content I\'ve Edited'),
    '#view' => 'workbench_edited',
    '#view_display' => 'block_1',
    '#attributes' => array('class' => array('right', 'clearfix')),
    '#theme' => 'workbench_element',
  );
  $output['workbench_recent_content'] = array(
    '#title' => t('All Recent Content'),
    '#view' => 'workbench_recent_content',
    '#view_display' => 'block_1',
    '#attributes' => array('class' => array('clearfix'), 'style' => array('clear: both;')),
    '#theme' => 'workbench_element',
  );

  // Allow other modules to alter the default page.
  drupal_alter('workbench_content', $output);

  // Transform the Views into markup.
  foreach (element_children($output) as $key) {
    $value = $output[$key];
    if (isset($value['#view'])) {
      $markup = '';
      if (isset($output[$key]['#markup'])) {
        $markup = $output[$key]['#markup'];
      }
      $view = $value['#view'];
      $display = isset($value['#view_display']) ? $value['#view_display'] : 'default';
      // TODO: pass arguments to the View.
      $output[$key]['#markup'] = $markup . views_embed_view($view, $display);
    }
  }

  return $output;
}

<?php

$plugin = array(
  'title' => t('Workbench Create'),
  'description' => t('Deploys an instance of the workbench create administration tool.'),
  'content form' => 'workbench_workbench_create_settings_form',
  'content form submit' => 'workbench_workbench_create_settings_form_submit',
  'render' => 'workbench_workbench_create_render',
);

function workbench_workbench_create_settings_form($form, $form_state) {
  return $form;
}

function workbench_workbench_create_settings_form_submit($form, &$form_state) {
}

function workbench_workbench_create_render() {
  module_load_include('inc', 'workbench', 'workbench.pages');
  return workbench_create();
}

/**
 * Simple page to show list of content type to create.
 *
 * @see hook_workbench_create_alter()
 *
 * @return
 *  A Render API array of content creation options.
 */
function workbench_create() {
  $output = array();
  $output['workbench_create_default'] = _workbench_create();

  // Allow other modules to add content here.
  drupal_alter('workbench_create', $output);

  return $output;
}

/**
 * Emulate node/add to return content creation list.
 *
 * @return
 *  A Render API array of content creation options.
 */
function _workbench_create() {
  $output = array();
  $output['#markup'] = '<h3>' . t('Create content') . '</h3>';

  $item = menu_get_item('node/add');
  $content = system_admin_menu_block($item);

  if (!empty($content)) {
    // Theme the arrray of content type creation links.
    $output['#markup'] .= theme('node_add_list', array('content' => $content));
  }
  else {
    $output['#markup'] .= t('You cannot create any new content. Please contact the site administrator.');
  }

  return $output;
}
